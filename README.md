WebMIDI/WebAudio Experiment
===========================

This is a WebMIDI experiment, built with Preact using HTM templates. You do not
need to "build" anything; the files you see are what the browser runs.

Opening `index.html` "directly" won't work -- browsers don't support JS modules
for `file://` URLs -- so you'll need to run a local web server. For example:

    ruby -run -e httpd . -p 9090

PS: Firefox doesn't (currently) support WebMIDI; use Chrome instead.

TODO
----

+ Add a sampler device
+ Add a drum machine device
+ Add "effects" devices (delay, reverb, etc.)
+ Fix looping so that it works 100% correctly.
+ Allow the synthesizer oscillators to specify semitone offsets
+ Synthesizer needs at least one LFO
+ Ability to add/remove devices
+ Ability to add/remove tracks
+ Piano roll editor
+ Drum editor
+ Ability to load device presets
+ Ability to load/save songs
+ Ability to export songs as WAV, MP3, etc.
+ MIDI input handling

License
-------

Copyright (c) 2021 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
