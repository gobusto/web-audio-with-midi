import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'

// Wraps some kind of input element with a text label.
function InputLabel (props) {
  return html`
    <label className='input-label'>
      <span className='input-label-text'>${props.label}</span>
      ${props.children}
    </label>
  `
}

export default InputLabel
