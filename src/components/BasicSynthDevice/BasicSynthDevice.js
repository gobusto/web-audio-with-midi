import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'
import Dropdown from '../Dropdown/Dropdown.js'
import InputLabel from '../InputLabel/InputLabel.js'
import RangeInput from '../RangeInput/RangeInput.js'

// This device essentially acts as a wrapper around a WebAudio OscillatorNode.
function BasicSynthDevice (props) {
  const options = {
    sawtooth: 'Saw',
    sine: 'Sine',
    square: 'Square',
    triangle: 'Triangle'
  }

  const filters = {
    lowpass: 'Lowpass',
    highpass: 'Highpass',
    bandpass: 'Bandpass',
    notch: 'Notch',
    allpass: 'Allpass'
  }

  return html`
    <div className='basic-synth-device'>

      <${InputLabel} label='Volume'>
        <${RangeInput}
          min=0
          max=100
          value=${props.device.volume}
          onChange=${e => props.onChange({ volume: e.target.value })}
        />
      </${InputLabel}>

      <${InputLabel} label='Pan'>
        <${RangeInput}
          min='-1'
          max='1'
          step='0.01'
          value=${props.device.pan}
          onChange=${e => props.onChange({ pan: e.target.value })}
        />
      </${InputLabel}>

      <hr />

      <${InputLabel} label='Oscillator #1 Enabled'>
        <input
          className='foo'
          type='checkbox'
          checked=${props.device.oscOneEnabled}
          onChange=${() => props.onChange({ oscOneEnabled: !props.device.oscOneEnabled })}
        />
      </${InputLabel}>

      <${InputLabel} label='Oscillator #1 Mode'>
        <${Dropdown}
          value=${props.device.oscOneMode}
          options=${options}
          onChange=${e => props.onChange({ oscOneMode: e.target.value })}
        />
      </${InputLabel}>

      <${InputLabel} label='Oscillator #1 Detune'>
        <${RangeInput}
          min=-100
          max=100
          value=${props.device.oscOneDetune}
          onChange=${e => props.onChange({ oscOneDetune: Number(e.target.value) })}
        />
      </${InputLabel}>

      <hr />

      <${InputLabel} label='Oscillator #2 Enabled'>
        <input
          className='foo'
          type='checkbox'
          checked=${props.device.oscTwoEnabled}
          onChange=${() => props.onChange({ oscTwoEnabled: !props.device.oscTwoEnabled })}
        />
      </${InputLabel}>

      <${InputLabel} label='Oscillator #2 Mode'>
        <${Dropdown}
          value=${props.device.oscTwoMode}
          options=${options}
          onChange=${e => props.onChange({ oscTwoMode: e.target.value })}
        />
      </${InputLabel}>

      <${InputLabel} label='Oscillator #2 Detune'>
        <${RangeInput}
          min=-100
          max=100
          value=${props.device.oscTwoDetune}
          onChange=${e => props.onChange({ oscTwoDetune: Number(e.target.value) })}
        />
      </${InputLabel}>

      <hr />

      <${InputLabel} label='Noise Level'>
        <${RangeInput}
          min=0
          max=100
          value=${props.device.noiseLevel}
          onChange=${e => props.onChange({ noiseLevel: Number(e.target.value) })}
        />
      </${InputLabel}>

      <hr />

      <${InputLabel} label='Filter Enabled'>
        <input
          className='foo'
          type='checkbox'
          checked=${props.device.filterEnabled}
          onChange=${() => props.onChange({ filterEnabled: !props.device.filterEnabled })}
        />
      </${InputLabel}>

      <${InputLabel} label='Filter Type'>
        <${Dropdown}
          value=${props.device.filterType}
          options=${filters}
          onChange=${e => props.onChange({ filterType: e.target.value })}
        />
      </${InputLabel}>

      <${InputLabel} label='Filter Frequency'>
        <${RangeInput}
          min=1
          max=14000
          value=${props.device.filterFrequency}
          onChange=${e => props.onChange({ filterFrequency: Number(e.target.value) })}
        />
      </${InputLabel}>

      <${InputLabel} label='Filter Resonance'>
        <${RangeInput}
          min=0.1
          max=10.0
          step=0.01
          value=${props.device.filterResonance}
          onChange=${e => props.onChange({ filterResonance: Number(e.target.value) })}
        />
      </${InputLabel}>

      <hr />

      <button onClick=${() => props.onSave('organic')}>
        Export as LMMS "Organic" File
      </button>

      <button onClick=${() => props.onSave('tripleoscillator')}>
        Export as LMMS "Triple Oscillator" File
      </button>

    </div>
  `
}

export default BasicSynthDevice
