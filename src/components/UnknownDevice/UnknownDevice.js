import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'

// This is used for instances where we don't have a more-specific component:
function UnknownDevice (props) {
  return html`
    <div className='unknown-device'>
      <div class='unknown-device-type'>${props.device.kind}</div>
    </div>
  `
}

export default UnknownDevice
