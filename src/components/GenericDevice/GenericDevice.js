import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'
import BasicSynthDevice from '../BasicSynthDevice/BasicSynthDevice.js'
import MidiOutDevice from '../MidiOutDevice/MidiOutDevice.js'
import UnknownDevice from '../UnknownDevice/UnknownDevice.js'

// Represents a generic device, of any kind; essentially a "wrapper" component.
function GenericDevice (props) {
  const classFor = { basicSynth: BasicSynthDevice, midiOut: MidiOutDevice }
  return html`<${classFor[props.device.kind] || UnknownDevice} ...${props} />`
}

export default GenericDevice
