import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'

// This component contains all of the "general" song controls, such as for BPM.
function ControlBar (props) {
  return html`
    <div className='control-bar'>
      <input
        type='number'
        min='10'
        max='999'
        value=${props.bpm}
        onChange=${props.onBpmChange}
      />
      ${props.playing ? html`
        <button onClick=${props.onStop}>
          Stop
        </button>
      ` : html`
        <button onClick=${props.onPlay}>
          Play
        </button>
      `}
      ${props.audioEnabled ? null : html`
        <button onClick=${props.onEnableAudio}>Enable WebAudio</button>
      `}
      ${props.midiEnabled ? null : html`
        <button onClick=${props.onEnableMidi}>Enable MIDI</button>
      `}
    </div>
  `
}

export default ControlBar
