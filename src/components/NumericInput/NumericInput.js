import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'

// A basic numeric input -- not a slider, dial, etc.
function NumericInput (props) {
  return html`
    <input
      className='numeric-input'
      type='number'
      min=${props.min}
      max=${props.max}
      value=${props.value}
      onChange=${props.onChange}
    />
  `
}

export default NumericInput
