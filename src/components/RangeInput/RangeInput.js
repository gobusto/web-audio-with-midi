import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'

// Basically a regular numeric input, but presented as a slider.
function RangeInput (props) {
  return html`
    <div className='range-input'>
      <input
        className='range-input-slider'
        type='range'
        min=${props.min || 0}
        max=${props.max || 127}
        step=${props.step || 1}
        value=${props.value}
        onInput=${props.onChange}
      />
      <span className='range-input-label'>${props.value}</span>
    </div>
  `
}

export default RangeInput
