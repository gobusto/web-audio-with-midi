import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'
import Dropdown from '../Dropdown/Dropdown.js'
import InputLabel from '../InputLabel/InputLabel.js'
import NumericInput from '../NumericInput/NumericInput.js'

// This device simply forwards all events on to a MIDI port.
function MidiOutDevice (props) {
  let currentPort = null
  const portsAvailable = { '': 'None' }

  if (props.midi) {
    props.midi.outputs.forEach(thisPort => {
      if (thisPort.id === props.device.port) { currentPort = thisPort }
      portsAvailable[thisPort.id] = thisPort.name
    })
  }

  return html`
    <div className='midi-out-device'>
      <${InputLabel} label='Port'>
        <${Dropdown}
          value=${props.device.port}
          options=${portsAvailable}
          onChange=${e => props.onChange({ port: e.target.value })}
        />
      </${InputLabel}>
      <${InputLabel} label='Channel'>
        <${NumericInput}
          min='0'
          max='15'
          value=${props.device.channel}
          onChange=${e => props.onChange({ channel: Number(e.target.value) })}
        />
      </${InputLabel}>
    </div>
  `
}

export default MidiOutDevice
