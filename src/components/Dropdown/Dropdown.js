import { html } from 'https://unpkg.com/htm/preact/index.mjs?module'

// Basically just a fancy <select> element.
function Dropdown (props) {
  const options = Object.keys(props.options || {}).map(key => html`
    <option key=${key} value=${key}>${props.options[key]}</option>
  `)

  return html`
    <select class='dropdown' value=${props.value} onChange=${props.onChange}>
      ${options}
    </select>
  `
}

export default Dropdown
