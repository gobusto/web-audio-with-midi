import { html, render } from 'https://unpkg.com/htm/preact/index.mjs?module'
import App from './App.js'

window.onload = () => render(html`<${App} />`, document.body)
