import sendMidi from './sendMidi.js'
import playSynth from './playSynth.js'

// Subdivisions per beat: https://en.wikipedia.org/wiki/Pulses_per_quarter_note
const PPQ = 480

// We have a "backlog" of events, so a "time buffer" helps smooth things out:
const TIME_BUFFER = 120

// Convert the event time from "PPQ" units to milliseconds:
function timeFrom (ppqTime, bpm) { return (ppqTime / PPQ) * (60 / bpm) * 1000 }

// Calculate how many PPQ will pass in a given amount of milliseconds:
function ppqFrom (msTime, bpm) { return (msTime / 1000) * (bpm / 60) * PPQ }

// Process all events scheduled to happen between the last update and now:
function processTrackEvents (lastTime, state, audio, midi) {
  // Calculate how far the "song position" has progressed since the last check:
  const now = window.performance.now()
  const pos = state.position + ppqFrom(now - (lastTime || now), state.bpm)

  state.tracks.forEach(track => {
    track.events.forEach(event => {
      // Skip any events which happen before "last time" or after "this time":
      if (event.startAt < state.position || event.startAt >= pos) { return }

      // Convert the event time from PPQ into number-of-milliseconds-from-now:
      const startTime = timeFrom(event.startAt - pos, state.bpm) + TIME_BUFFER
      const stopTime = timeFrom(event.stopAt - pos, state.bpm) + TIME_BUFFER

      // Send the event to any relevant device(s):
      state.devices.forEach(device => {
        if (!track.outputs.includes(device.id)) {
          return
        } else if (device.kind == 'basicSynth') {
          const synth = playSynth(audio, device, event, startTime, stopTime)
          if (synth) { synth.connect(audio.destination) } // or other devices!
        } else if (device.kind == 'midiOut') {
          sendMidi(midi, device, event, startTime, stopTime)
        }
      })

      // Debug:
      // console.log(startTime, stopTime, event)
    })
  })

  // Ensure that we loop back to where we're suppose to if we progress too far:
  if (state.loop && pos >= state.loopEnd) {
    return { time: now, position: state.loopStart }
  }

  return { time: now, position: pos }
}

export default processTrackEvents
