import buildOrganic from './xpf/buildOrganic.js'
import buildTripleOscillator from './xpf/buildTripleOscillator.js'

// Build the "wrapper" element for the actual `<organic>` (or whatever) node.
function buildInstrument (doc, kind, device) {
  const instrument = doc.createElement('instrument')
  instrument.setAttribute('name', kind)

  if (kind === 'organic') {
    instrument.appendChild(buildOrganic(doc, device))
  } else if (kind === 'tripleoscillator') {
    instrument.appendChild(buildTripleOscillator(doc, device))
  }

  return instrument
}

function buildElNode (kind, doc) {
  const elNode = doc.createElement(kind)

  elNode.append('lspd_syncmode', '0')
  elNode.append('lspd_numerator', '4')
  elNode.append('latt', '0')
  elNode.append('lspd_denominator', '4')
  elNode.append('sustain', '0.5')
  elNode.append('x100', '0')
  elNode.append('dec', '0.5')
  elNode.append('amt', '0')
  elNode.append('att', '0')
  elNode.append('ctlenvamt', '0')
  elNode.append('lpdel', '0')
  elNode.append('lshp', '0')
  elNode.append('lspd', '0.1')
  elNode.append('lamt', '0')
  elNode.append('userwavefile', '')
  elNode.append('rel', '0.1')
  elNode.append('pdel', '0')
  elNode.append('hold', '0.5')

  return elNode
}

function buildElData (doc, device) {
  const elData = doc.createElement('eldata')

  // Filters are referred to by ID numbers, rather than names:
  const filterID = {
    'lowpass': 0,
    'highpass': 1,
    'bandpass': 2, // CSG
    // 'bandpass_czpg': 3,
    'notch': 4,
    'allpass': 5
  }

  elData.setAttribute('fwet', device.filterEnabled ? '1' : '0')
  elData.setAttribute('ftype', String(filterID[device.filterType] || 0))
  elData.setAttribute('fcut', String(device.filterFrequency || 1))
  elData.setAttribute('fres', String(device.filterResonance || 0.01))

  elData.appendChild(buildElNode('elvol', doc))
  elData.appendChild(buildElNode('elcut', doc))
  elData.appendChild(buildElNode('elres', doc))

  return elData
}

// Build a `<chordcreator>` node with default settings.
function buildChordCreator (doc) {
  const chordCreator = doc.createElement('chordcreator')

  chordCreator.setAttribute('chord-enabled', '0')
  chordCreator.setAttribute('chordrange', '1')
  chordCreator.setAttribute('chord', '0')

  return chordCreator
}

// Build an `<arpeggiator>` node with default settings.
function buildArpeggiator (doc) {
  const arpeggiator = doc.createElement('arpeggiator')

  arpeggiator.setAttribute('arpmiss', '0')
  arpeggiator.setAttribute('arpmode', '0')
  arpeggiator.setAttribute('arpskip', '0')
  arpeggiator.setAttribute('arptime_denominator', '4')
  arpeggiator.setAttribute('arpdir', '0')
  arpeggiator.setAttribute('arpcycle', '0')
  arpeggiator.setAttribute('arptime', '200')
  arpeggiator.setAttribute('arp', '0')
  arpeggiator.setAttribute('arpgate', '100')
  arpeggiator.setAttribute('arp-enabled', '0')
  arpeggiator.setAttribute('arptime_syncmode', '0')
  arpeggiator.setAttribute('arptime_numerator', '4')
  arpeggiator.setAttribute('arprange', '1')

  return arpeggiator
}

// Build a `<midiport>` node with default settings.
function buildMidiPort (doc) {
  const midiPort = doc.createElement('midiport')

  midiPort.setAttribute('inputcontroller', '0')
  midiPort.setAttribute('outputcontroller', '0')
  midiPort.setAttribute('inputchannel', '0')
  midiPort.setAttribute('fixedoutputnote', '-1')
  midiPort.setAttribute('basevelocity', '63')
  midiPort.setAttribute('readable', '0')
  midiPort.setAttribute('writable', '0')
  midiPort.setAttribute('outputchannel', '1')
  midiPort.setAttribute('fixedinputvelocity', '-1')
  midiPort.setAttribute('fixedoutputvelocity', '-1')
  midiPort.setAttribute('outputprogram', '1')

 return midiPort
}

// Build an `<fxchain>` node with default settings.
function buildFxChain (doc) {
  const fxChain = doc.createElement('fxchain')

  fxChain.setAttribute('enabled', '0')
  fxChain.setAttribute('numofeffects', '0')

  return fxChain
}

// Build the main `<instrumenttrack>` node, including general device settings.
function buildInstrumentTrack (doc, kind, device) {
  const instrumentTrack = doc.createElement('instrumenttrack')

  instrumentTrack.setAttribute('usemasterpitch', '1')
  instrumentTrack.setAttribute('fxch', '0')
  instrumentTrack.setAttribute('vol', String(device.volume || 100))
  instrumentTrack.setAttribute('pan', String((device.pan || 0) * 100))
  instrumentTrack.setAttribute('basenote', '57')
  instrumentTrack.setAttribute('pitch', '0')
  instrumentTrack.setAttribute('pitchrange', '1')

  instrumentTrack.appendChild(buildInstrument(doc, kind, device))
  instrumentTrack.appendChild(buildElData(doc, device))
  instrumentTrack.appendChild(buildChordCreator(doc))
  instrumentTrack.appendChild(buildArpeggiator(doc))
  instrumentTrack.appendChild(buildMidiPort(doc))
  instrumentTrack.appendChild(buildFxChain(doc))

  return instrumentTrack
}

function buildInstrumentTrackSettings (doc, kind, device) {
  const instrumentTrackSettings = doc.createElement('instrumenttracksettings')

  instrumentTrackSettings.setAttribute('name', 'Default Name') // TODO
  instrumentTrackSettings.setAttribute('type', '0')
  instrumentTrackSettings.setAttribute('solo', '0')
  instrumentTrackSettings.setAttribute('muted', '0')

  instrumentTrackSettings.appendChild(buildInstrumentTrack(doc, kind, device))

  return instrumentTrackSettings
}

function buildLmmsProject (doc, kind, device) {
  const lmmsProject = doc.createElement('lmms-project')

  lmmsProject.setAttribute('type', 'instrumenttracksettings')
  lmmsProject.setAttribute('version', '1.0')
  lmmsProject.setAttribute('creator', 'PYSITC') // TODO
  lmmsProject.setAttribute('creatorversion', '0.1') // TODO

  lmmsProject.appendChild(doc.createElement('head'))
  lmmsProject.appendChild(buildInstrumentTrackSettings(doc, kind, device))

  return lmmsProject
}

// Generate an XPF file for the LMMS "Organic" synthesizer from a synth device.
function generateXPF (device, kind) {
  // Only a synthesizer device can export its settings as an XPF:
  if (!device || device.kind !== 'basicSynth') { return null }
  // LMMS supports a few different kinds of synthesizer, so pick one:
  if (kind !== 'organic' && kind !== 'tripleoscillator') { return null }

  // Create a new XML document, with a custom doctype:
  const dt = document.implementation.createDocumentType('lmms-project', '', '')
  const doc = document.implementation.createDocument(null, null, dt)
  doc.appendChild(buildLmmsProject(doc, kind, device))

  // Always add an `<?xml` header: https://stackoverflow.com/a/16725572/4200092
  const xmlGenerator = new XMLSerializer()
  const xmlText = xmlGenerator.serializeToString(doc)
  if (xmlText.startsWith('<?xml')) { return xmlText }
  return ['<?xml version="1.0"?>', xmlText].join('\n')
}

export default generateXPF
