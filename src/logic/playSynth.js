import generateWhiteNoise from './generateWhiteNoise.js'

// See https://en.wikipedia.org/wiki/Piano_key_frequencies for the formula, but
// be aware that MIDI numbers the notes differently to the standard piano ones:
// https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
const f = (n) => Math.pow(2, (n - 69) / 12) * 440 // A4 = Piano #49 or MIDI #69

// Play a "note" event by building the necessary oscillator (and other!) nodes.
function playSynth (audio, device, event, startTime, endTime) {
  if (!audio) { return null }

  // Oscillator #1:
  const osc1 = device.oscOneEnabled ? new OscillatorNode(audio, {
    type: device.oscOneMode || 'square',
    detune: device.oscOneDetune || 0,
    frequency: f(event.note)
  }) : null

  // Oscillator #2:
  const osc2 = device.oscTwoEnabled ? new OscillatorNode(audio, {
    type: device.oscTwoMode || 'square',
    detune: device.oscTwoDetune || 0,
    frequency: f(event.note)
  }) : null

  // Noise channel:
  const noise = device.noiseLevel ? new AudioBufferSourceNode(audio, {
    loop: true,
    buffer: generateWhiteNoise(audio)
  }) : null

  // If everything is currently switched off, then we don't do anything:
  if (!osc1 && !osc2 && !noise) { return null }

  // We also allow the gain and pan values to be set via some additional nodes:
  const vol = (device.volume || 100) / 100
  const panNode = new StereoPannerNode(audio, { pan: device.pan || 0 })
  const gainNode = new GainNode(audio, { gain: (event.velocity / 127) * vol })
  gainNode.connect(panNode)

  // Play the actual note:
  const timing = audio.currentTime + startTime/1000

  if (osc1) {
    osc1.connect(gainNode)
    osc1.start(timing)
    osc1.stop(audio.currentTime + endTime/1000)
  }

  if (osc2) {
    osc2.connect(gainNode)
    osc2.start(timing)
    osc2.stop(audio.currentTime + endTime/1000)
  }

  if (noise) {
    const noiseVolume = new GainNode(audio, { gain: device.noiseLevel / 100 })
    noise.connect(noiseVolume)
    noiseVolume.connect(gainNode)

    noise.start(timing)
    noise.stop(audio.currentTime + endTime/1000)
  }

  // Return the final node in the chain, in case we want to connect more nodes:
  if (!device.filterEnabled) { return panNode }

  // Add a filter, if necessary:
  const filterNode = new BiquadFilterNode(audio, {
    type: device.filterType || 'lowpass',
    frequency: device.filterFrequency || 1,
    Q: device.filterResonance || 0.01
  })

  panNode.connect(filterNode)
  return filterNode
}

export default playSynth
