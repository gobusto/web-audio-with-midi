function sendMidi (midi, device, event, startTime, stopTime) {
  if (!midi) { return } // If we don't have MIDI access, we can't do anything.

  // This SHOULD never happen... but just in case:
  if (device.channel < 0 || device.channel > 15) { return }

  midi.outputs.forEach(port => {
    if (port.id !== device.port) { return }

    // TODO: Handle things other than note events (such as pitch wheel).
    port.send([0x90 + device.channel, event.note, event.velocity], window.performance.now() + startTime)
    port.send([0x80 + device.channel, event.note, event.velocity], window.performance.now() + stopTime)
  })
}

export default sendMidi
