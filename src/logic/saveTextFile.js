// Save a plain-text string to a file (no Base64 encoding).
function saveTextFile (text, settings) {
  const fileName = (settings || {}).fileName || 'download.txt'
  const mimeType = (settings || {}).mimeType || 'text/plain'

  const a = document.createElement('a')
  a.download = fileName
  a.href = `data:${mimeType},${text}`
  a.click()
}

export default saveTextFile
