/*
<?xml version="1.0"?>
<!DOCTYPE lmms-project>
<lmms-project creatorversion="1.2.2" type="instrumenttracksettings" creator="LMMS" version="1.0">
  <head/>
  <instrumenttracksettings type="0" solo="0" name="Two Squares" muted="0">
    <instrumenttrack basenote="57" vol="35.200001" usemasterpitch="1" pan="0" pitch="0" pitchrange="1" fxch="0">
      <instrument name="organic">
        <organic vol0="100" pan2="0" vol3="0" newharmonic1="1" newharmonic2="2" newdetune0="-10" foldback="0" vol="100" newdetune7="0" pan3="0" pan5="0" newdetune3="0" newharmonic6="6" wavetype2="0" newharmonic4="4" newharmonic3="3" wavetype1="2" wavetype3="0" pan4="0" newharmonic0="2" wavetype4="0" vol6="0" wavetype0="2" newdetune4="0" wavetype6="0" pan0="0" wavetype5="0" newdetune1="10" vol7="0" vol4="0" vol2="0" newdetune5="0" vol1="100" pan1="0" newharmonic5="5" wavetype7="0" num_osc="8" newdetune6="0" newdetune2="0" pan7="0" pan6="0" vol5="0" newharmonic7="7"/>
      </instrument>
      <eldata ftype="0" fres="10" fwet="0" fcut="14000">
        <elvol lspd_syncmode="8" lspd_numerator="1" latt="0" lspd_denominator="3" sustain="0" x100="0" dec="0" amt="0" att="0" ctlenvamt="0" lpdel="0" lshp="0" lspd="0.0071428572" lamt="1" userwavefile="" rel="0" pdel="0" hold="0"/>
        <elcut lspd_syncmode="0" lspd_numerator="4" latt="0" lspd_denominator="4" sustain="0.5" x100="0" dec="0.5" amt="0" att="0" ctlenvamt="0" lpdel="0" lshp="0" lspd="0.1" lamt="0" userwavefile="" rel="0.1" pdel="0" hold="0.5"/>
        <elres lspd_syncmode="0" lspd_numerator="4" latt="0" lspd_denominator="4" sustain="0.5" x100="0" dec="0.5" amt="0" att="0" ctlenvamt="0" lpdel="0" lshp="0" lspd="0.1" lamt="0" userwavefile="" rel="0.1" pdel="0" hold="0.5"/>
      </eldata>
      <chordcreator chordrange="1" chord="0" chord-enabled="0"/>
      <arpeggiator arpmiss="0" arpmode="0" arpskip="0" arptime_denominator="4" arpdir="0" arpcycle="0" arptime="200" arp="0" arpgate="100" arp-enabled="0" arptime_syncmode="0" arptime_numerator="4" arprange="1"/>
      <midiport inputcontroller="0" outputcontroller="0" inputchannel="0" fixedoutputnote="-1" basevelocity="63" readable="0" writable="0" outputchannel="1" fixedinputvelocity="-1" fixedoutputvelocity="-1" outputprogram="1"/>
      <fxchain enabled="0" numofeffects="0"/>
    </instrumenttrack>
  </instrumenttracksettings>
</lmms-project>
*/

// Add attributes for a single oscillator to an existing `<organic>` DOM node.
function addOrganicAttributes (node, oscillatorNumber, data) {
  // Waves are referred to by ID numbers, rather than names -- here's the list:
  const waveTypeID = {
    'sine': 0,
    'sawtooth': 1,
    'square': 2,
    'triangle': 3,
    // These two are not supported by WebAudio, but are here for completeness:
    'moogsaw': 4,
    'exponential': 5
  }

  const waveType = String(waveTypeID[data.waveType] || 0)
  node.setAttribute(`wavetype${oscillatorNumber}`, waveType)

  node.setAttribute(`vol${oscillatorNumber}`, String(data.volume || 0))
  node.setAttribute(`pan${oscillatorNumber}`, '0')

  node.setAttribute(`newharmonic${oscillatorNumber}`, '2') // "Fundamental"
  node.setAttribute(`newdetune${oscillatorNumber}`, String(data.detune || 0))
}

// Build a (device-specific) settings node for the LMMS "organic" synthesizer.
function buildOrganic (doc, device) {
  const organic = doc.createElement('organic')

  // This is distinct from the "main" volume defined by `<instrumenttrack>`:
  organic.setAttribute('vol', '100')

  // Our synth doesn't have a built-in distortion effect, so leave this as 0:
  organic.setAttribute('foldback', '0')

  // I'm not sure if this can be changed, so let's stick with the default of 8:
  organic.setAttribute('num_osc', '8')

  // First oscillator:
  addOrganicAttributes(organic, 0, {
    waveType: device.oscOneMode,
    detune: device.oscOneDetune,
    volume: device.oscOneEnabled ? 100 : 0
  })

  // Second oscillator:
  addOrganicAttributes(organic, 1, {
    waveType: device.oscTwoMode,
    detune: device.oscTwoDetune,
    volume: device.oscTwoEnabled ? 100 : 0
  })

  // The other 6 are not used:
  addOrganicAttributes(organic, 2, {})
  addOrganicAttributes(organic, 3, {})
  addOrganicAttributes(organic, 4, {})
  addOrganicAttributes(organic, 5, {})
  addOrganicAttributes(organic, 6, {})
  addOrganicAttributes(organic, 7, {})

  return organic
}

export default buildOrganic
