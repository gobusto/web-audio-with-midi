/*
<?xml version="1.0"?>
<!DOCTYPE lmms-project>
<lmms-project version="1.0" type="instrumenttracksettings" creator="LMMS" creatorversion="1.2.2">
  <head/>
  <instrumenttracksettings solo="0" name="TripleOscillator" muted="0" type="0">
    <instrumenttrack pitchrange="1" vol="100" usemasterpitch="1" pitch="0" basenote="57" pan="0" fxch="0">
      <instrument name="tripleoscillator">
        <tripleoscillator userwavefile1="" pan2="0" phoffset0="0" vol1="33" stphdetun2="0" modalgo3="2" finer0="0" phoffset2="0" wavetype2="6" wavetype0="4" coarse1="-12" modalgo2="2" finel2="0" pan0="0" finel1="0" coarse2="-24" pan1="0" finel0="0" modalgo1="2" vol0="33" wavetype1="5" userwavefile0="" coarse0="0" stphdetun1="0" finer1="0" stphdetun0="0" phoffset1="0" vol2="33" finer2="0" userwavefile2=""/>
      </instrument>
      <eldata fcut="14000" ftype="0" fres="0.5" fwet="0">
        <elvol latt="0" ctlenvamt="0" sustain="0.5" lspd="0.1" lspd_denominator="4" x100="0" rel="0.1" lamt="0" amt="0" userwavefile="" att="0" lspd_syncmode="0" lshp="0" lspd_numerator="4" hold="0.5" pdel="0" lpdel="0" dec="0.5"/>
        <elcut latt="0" ctlenvamt="0" sustain="0.5" lspd="0.1" lspd_denominator="4" x100="0" rel="0.1" lamt="0" amt="0" userwavefile="" att="0" lspd_syncmode="0" lshp="0" lspd_numerator="4" hold="0.5" pdel="0" lpdel="0" dec="0.5"/>
        <elres latt="0" ctlenvamt="0" sustain="0.5" lspd="0.1" lspd_denominator="4" x100="0" rel="0.1" lamt="0" amt="0" userwavefile="" att="0" lspd_syncmode="0" lshp="0" lspd_numerator="4" hold="0.5" pdel="0" lpdel="0" dec="0.5"/>
      </eldata>
      <chordcreator chordrange="1" chord="0" chord-enabled="0"/>
      <arpeggiator arp="0" arptime_denominator="4" arp-enabled="0" arpmode="0" arptime_numerator="4" arprange="1" arpcycle="0" arptime="100" arpgate="100" arpdir="0" arpmiss="0" arpskip="0" arptime_syncmode="0"/>
      <midiport writable="0" fixedinputvelocity="-1" fixedoutputvelocity="-1" outputchannel="1" readable="0" inputchannel="0" fixedoutputnote="-1" outputprogram="1" inputcontroller="0" outputcontroller="0" basevelocity="63"/>
      <fxchain numofeffects="0" enabled="0"/>
    </instrumenttrack>
  </instrumenttracksettings>
</lmms-project>
*/

// Add attributes for a single oscillator to a `<tripleoscillator>` DOM node.
function addTripleOscillatorAttributes (node, oscillatorNumber, data) {
  // Waves are referred to by ID numbers, rather than names -- here's the list:
  const waveTypeID = {
    'sine': 0,
    'triangle': 1,
    'sawtooth': 2,
    'square': 3,
    'moogsaw': 4,
    'exponential': 5,
    'noise': 6,
    'user': 7
  }

  const waveType = String(waveTypeID[data.waveType] || 0)
  node.setAttribute(`wavetype${oscillatorNumber}`, waveType)
  node.setAttribute(`userwavefile${oscillatorNumber}`, '')

  node.setAttribute(`vol${oscillatorNumber}`, String(data.volume || 0))
  node.setAttribute(`pan${oscillatorNumber}`, '0')

  node.setAttribute(`coarse${oscillatorNumber}`, '0')
  node.setAttribute(`finel${oscillatorNumber}`, String(data.detune || 0))
  node.setAttribute(`finer${oscillatorNumber}`, String(data.detune || 0))

  node.setAttribute(`phoffset${oscillatorNumber}`, '0')
  node.setAttribute(`stphdetun${oscillatorNumber}`, '0')
}

// Build a (device-specific) settings node for the LMMS "triple oscillator".
function buildTripleOscillator (doc, device) {
  const tripleOscillator = doc.createElement('tripleoscillator')

  // OSC1 + OSC2 interaction:
  tripleOscillator.setAttribute('modalgo1', '2') // 2 = "Mix"

  // OSC2 + OSC3 interaction:
  tripleOscillator.setAttribute('modalgo2', '2') // 2 = "Mix"

  // OSC1 + OSC3...? Not shown in UI:
  tripleOscillator.setAttribute('modalgo3', '2') // 2 = "Mix"

  // First oscillator:
  addTripleOscillatorAttributes(tripleOscillator, 0, {
    waveType: device.oscOneMode,
    detune: device.oscOneDetune,
    volume: device.oscOneEnabled ? 100 : 0
  })

  // Second oscillator:
  addTripleOscillatorAttributes(tripleOscillator, 1, {
    waveType: device.oscTwoMode,
    detune: device.oscTwoDetune,
    volume: device.oscTwoEnabled ? 100 : 0
  })

  // Noise generator:
  addTripleOscillatorAttributes(tripleOscillator, 2, {
    waveType: 'noise',
    volume: device.noiseLevel
  })

  return tripleOscillator
}

export default buildTripleOscillator
