let whiteNoise = null

// See https://noisehack.com/generate-noise-web-audio-api/
function generateWhiteNoise (audioContext) {
  // Re-use the previously-generated noise data if possible:
  if (whiteNoise) { return whiteNoise }

  // Otherwise, generate a new noise buffer...
  whiteNoise = new AudioBuffer({
    numberOfChannels: 1,
    sampleRate: audioContext.sampleRate,
    length: audioContext.sampleRate * 2 // 2 seconds long.
  })

  // ...and fill it with white noise:
  const buf = whiteNoise.getChannelData(0)
  for (let i = 0; i < whiteNoise.length; ++i) { buf[i] = Math.random() * 2 - 1 }
  return whiteNoise
}

export default generateWhiteNoise
