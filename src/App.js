import { Component, html } from 'https://unpkg.com/htm/preact/index.mjs?module'

import ControlBar from './components/ControlBar/ControlBar.js'
import GenericDevice from './components/GenericDevice/GenericDevice.js'
import Track from './components/Track/Track.js'

import generateXPF from './logic/generateXPF.js'
import processEvents from './logic/processEvents.js'
import saveTextFile from './logic/saveTextFile.js'

// writeme
class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      bpm: 120,
      playing: false,
      loop: true,
      loopStart: 0,
      loopEnd: 480 * 4,
      // TODO: Could this be an Object, rather than an Array...?
      devices: [
        {
          id: 'aUniqueID',
          kind: 'midiOut',
          // Per-device settings:
          port: null,
          channel: 0
        },
        {
          id: 'anotherUniqueID',
          kind: 'basicSynth',
          // Per-device settings:
          oscOneEnabled: true,
          oscOneMode: 'sawtooth',
          oscOneDetune: -5,
          oscTwoEnabled: true,
          oscTwoMode: 'square',
          oscTwoDetune: 5,
          noiseLevel: 20,
          filterEnabled: true,
          filterType: 'lowpass',
          filterFrequency: 3600,
          filterResonance: 5,
          pan: 0,
          volume: 33
        },
        {
          id: 'yetAnotherUniqueID',
          kind: 'fakeDevice'
        }
      ],
      tracks: [
        {
          drum: false, // Determines if we are a Piano Roll or Drum Track view.
          outputs: [
            'aUniqueID',
            'anotherUniqueID',
          ],
          events: [
            {
              kind: 'note',
              startAt: 0 * 480,
              stopAt: 1 * 480,
              note: 60,
              velocity: 64
            },
            {
              kind: 'note',
              startAt: 1 * 480,
              stopAt: 2 * 480,
              note: 64,
              velocity: 96
            },
            {
              kind: 'note',
              startAt: 2 * 480,
              stopAt: 3 * 480,
              note: 67,
              velocity: 127
            }
          ]
        }
      ]
    }
  }

  componentDidMount () {
    this.clockTick()
  }

  componentWillUnmount () {
    window.clearTimeout(this.timerId)
  }

  clockTick = () => {
    this.timerId = window.setTimeout(this.clockTick)
    if (!this.state.playing) { return }

    const result = processEvents(this.timer, this.state, this.audio, this.midi)
    this.timer = result.time
    this.setState({ position: result.position })
  }

  handlePlay = () => {
    this.timer = null
    this.setState({ playing: true, position: 0 })
  }

  // TODO: Send a "stop all notes" message to each (MIDI?) device here.
  handleStop = () => {
    this.setState({ playing: false })
  }

  handleBpmChange = (event) => {
    let value = Number(event.target.value)
    value = Math.max(10, value)
    value = Math.min(999, value)
    this.setState({ bpm: value })
  }

  handleEnableAudio = () => {
    if (this.audio) { return }

    this.audio = new AudioContext()
    this.setState({ audioEnabled: true })
  }

  handleEnableMidi = () => {
    if (this.midi) { return }

    if (navigator.requestMIDIAccess) {
      navigator.requestMIDIAccess().then(
        midi => { this.midi = midi, this.setState({ midiEnabled: true }) },
        error => console.log('MIDI access was denied:', error)
      )
    } else {
      console.log('This browser does not support MIDI access.')
    }
  }

  handleDeviceChange = (device, newData) => {
    // console.log('Device Change:', device.id, newData)

    const newDevices = this.state.devices.map(thisDevice => (
      thisDevice.id === device.id ? { ...thisDevice, ...newData } : thisDevice
    ))

    this.setState({ devices: newDevices })
  }

  handleSavePreset = (device, format) => {
    const text = generateXPF(device, format)
    if (!text) { return alert(`Could not export format: ${format}`) }

    saveTextFile(text, { fileName: `${format}.xpf`, mimeType: 'text/xml' })
  }

  render () {
    const devices = this.state.devices.map(device => html`
      <${GenericDevice}
        key=${device.id}
        midi=${this.midi}
        device=${device}
        onChange=${data => this.handleDeviceChange(device, data)}
        onSave=${format => this.handleSavePreset(device, format)}
      />
    `)

    const tracks = this.state.tracks.map((track, key) => html`
      <${Track} key=${key} track=${track} />
    `)

    return html`
      <div className='app'>
        <${ControlBar}
          playing=${this.state.playing}
          onPlay=${this.handlePlay}
          onStop=${this.handleStop}
          bpm=${this.state.bpm}
          onBpmChange=${this.handleBpmChange}
          audioEnabled=${this.state.audioEnabled}
          onEnableAudio=${this.handleEnableAudio}
          midiEnabled=${this.state.midiEnabled}
          onEnableMidi=${this.handleEnableMidi}
        />
        <div className='devices'>
          ${devices}
        </div>
        <div className='tracks'>
          ${tracks}
        </div>
      </div>
    `
  }
}

export default App
